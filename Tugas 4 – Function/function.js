//soal 1
console.log("          SOAL 1           ")
console.log("========== HASIL ==========")

function teriak(){
    var text = "Halo Sanbers !"
    return text
}

console.log(teriak())

console.log("===========================")

//soal 2
console.log("          SOAL 2           ")
console.log("========== HASIL ==========")

function kalikan(num1,num2){
    var num1
    var num2

    hasil = num1 * num2;
    return hasil
}
var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48

console.log("===========================")

//soal 3
console.log("          SOAL 3           ")
console.log("========== HASIL ==========")

function introduce(name,age,address,hobby){
    var name
    var age
    var address
    var hobby

    var text ="Nama saya "+name+", umur saya "+age+" tahun, alamat saya di "+address+", dan saya punya hobby yaitu "+hobby
    return text;
}

var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)
console.log("===========================")